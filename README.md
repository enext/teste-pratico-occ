# enext :: teste prático OCC

Primeiramente parabéns por ter chegado até aqui. Agora vamos avaliar o seu conhecimento por meio deste teste que consiste em desenvolver uma página de detalhe do produto para os kits de produtos do cliente XPTO.

# descrição da demanda
O cliente XPTO tem alguns kits de produtos e precisa de uma página de produto específica para esses produtos a fim de aumentar as vendas dos mesmos. Para isso, foi desenvolvida uma página onde deixa evidente a economia obtida ao se comprar o kit comparada ao valor da soma dos preços dos itens que o compõem comprados separadamente.

# requisitos
- Geral
	- Utilizar o Git como controlador de versões
- Backend
	- Desenvolver uma aplicação REST + JSON em NodeJS 10+
- Frontend
	- Desenvolver a página proposta utilizando algum framework que trabalhe com o conceito de MVVM¹

¹ o OCC utiliza o **KnockoutJS**, utilizá-lo será um diferencial.

# o problema dos kits
Um kit na plataforma OCC é um SKU composto por vários SKUs que são vendidos como um único item. Acontece que na resposta da API padrão de detalhes, tanto do produto como do sku, não são devolvidos quais são os SKUs que estão naquele kit.

# a solução
Para resolver este problema é necessário desenvolver um endpoint onde devolva quais os SKUs contidos em determinado kit. Essa informação é devolvida no endpoint **getSku** na **Admin API**

# o que devo entregar?
- Link do repositório no bitbucket com o teste e instruções de como rodá-lo
	- Frontend: a página de detalhes do kit (layout enviado) que consome as informações dos enpoints que serão desenvolvidos no backend
	- Backend: a aplicação que contenha os endpoints necessários para a apresentação da página.

# informações para desenvolvimento

- O layout está na raiz do repositório salvo como layout.png
- ID do produto: kit-camisas-dudalina
- SKU do produto: kit-camisas-dudalina
- URL Base: https://ccadmin-z9ta.oracleoutsourcing.com/
- Documentação OCC https://docs.oracle.com/cd/E97801_04/Cloud.19B/UsingCC/html/s0101understandoraclecommercecloud01.html
- Documentação da API https://docs.oracle.com/en/cloud/saas/commerce-cloud/cxocc/index.html
- API key: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMGUwYjlkNS1lYWE0LTQ1MmQtYjc3My1iYmQ0MDI2N2ExNDkiLCJpc3MiOiJhcHBsaWNhdGlvbkF1dGgiLCJleHAiOjE1OTU3MTc3MzgsImlhdCI6MTU2NDE4MTczOH0=.VFlr+wpaPPreyPlm/t4S+O+FX0MTReIIh7/IM6c79tk=

# dicas
- As APIs do OCC são divididas em 3 grupos básicos: Agent API (não será necessária para o teste), Admin API e Storefront API.
- As chamadas para a Storefront API também precisarão do token de autenticação
#### Exemplo
```HTTP
GET /ccstore/v1/skus/d1099Cd82a22D051 HTTP/1.1
Host: ccadmin-z9ta.oracleoutsourcing.com
Authorization: Bearer {{token}}
...
```
# Boa sorte!